package com.example.hieptran.cameraexample.filter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

import com.example.hieptran.cameraexample.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by hieptran on 04/12/2017.
 * TODO: Demo cach su dung mot filter, duoc render tu GL
 * Dang xu ly
 */

public class HiepTHbTestFilter extends BaseFilter {
    private static final String TAG = "HiepTHbDemo";
    /** Size of the texture coordinate data in elements. */
    private final int mTextureCoordinateDataSize = 2;
    /** This is a handle to our texture data. */
    private int mTextureDataHandle0;
    private int mTextureDataHandle1;
    private final FloatBuffer mCubeTextureCoordinates;
    static final float TEXTURE_COORDS[] = {
            1.0f, 0.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
    };
    static FloatBuffer VERTEX_BUF, TEXTURE_COORD_BUF;
    public HiepTHbTestFilter (Context mActivityContext, Resources resources) {
        //Ham khoi tao tu resource
        super(NO_FILTER_VERTEX_SHADER, OpenGlUtils.readShaderFromRawResource(resources, R.raw.blurring_fragment_shader));
        if (TEXTURE_COORD_BUF == null) {
            TEXTURE_COORD_BUF = ByteBuffer.allocateDirect(TEXTURE_COORDS.length * 4)
                    .order(ByteOrder.nativeOrder()).asFloatBuffer();
            TEXTURE_COORD_BUF.put(TEXTURE_COORDS);
            TEXTURE_COORD_BUF.position(0);
        }
        mCubeTextureCoordinates = ByteBuffer
                .allocateDirect(
                        TEXTURE_COORDS.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubeTextureCoordinates.put(TEXTURE_COORDS).position(0);
        // Load the texture
       /* mTextureDataHandle0 = loadTexture(mActivityContext,
                R.drawable.picture2);

        // Load the texture
        mTextureDataHandle1 = loadTexture(mActivityContext,
                R.drawable.picture3);*/
    }

    @Override
    public void onOutputSizeChanged(int width, int height) {
        super.onOutputSizeChanged(width, height);

    }

    @Override
    public void onInit() {
        super.onInit();
       setTexCoord();
       setChannel();
    }

    private void setTexCoord() {
        int mTextureCoordinateHandle = GLES20.glGetUniformLocation(getProgram(),
                "v_TexCoordinate");
        mCubeTextureCoordinates.position(0);
        Log.d(TAG, "setTexCoord: mTextureCoordinateHandle="+mTextureCoordinateHandle);
        GLES20.glVertexAttribPointer(mTextureCoordinateHandle,
                mTextureCoordinateDataSize, GLES20.GL_FLOAT, false, 0,
                mCubeTextureCoordinates);

        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);

    }
    private void setChannel() {
        int  mTextureUniformHandle0 = GLES20.glGetUniformLocation(getProgram(),
                "u_Texture0");
        int mTextureUniformHandle1 = GLES20.glGetUniformLocation(getProgram(),
                "u_Texture1");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        GLES20.glUniform1i(mTextureUniformHandle0, 0);
        GLES20.glActiveTexture(GLES20.GL_TEXTURE1);

        // Bind the texture to this unit.
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 1);

        GLES20.glUniform1i(mTextureUniformHandle1, 1);
    }

    public static int loadTexture(final Context context, final int resourceId)
    {
        final int[] textureHandle = new int[1];

        GLES20.glGenTextures(1, textureHandle, 0);

        if (textureHandle[0] != 0)
        {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;	// No pre-scaling

            // Read in the resource
            final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

            // Bind to the texture in OpenGL
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            // Set filtering
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

            // Load the bitmap into the bound texture.
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            // Recycle the bitmap, since its data has been loaded into OpenGL.
            bitmap.recycle();
        }

        if (textureHandle[0] == 0)
        {
            throw new RuntimeException("Error loading texture.");
        }

        return textureHandle[0];
    }
}
