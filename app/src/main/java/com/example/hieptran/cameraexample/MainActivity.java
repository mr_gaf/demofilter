package com.example.hieptran.cameraexample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.widget.ImageView;

import com.example.hieptran.cameraexample.filter.BaseFilter;
import com.example.hieptran.cameraexample.filter.BeautyFilter;
import com.example.hieptran.cameraexample.filter.FilterRender;
import com.example.hieptran.cameraexample.filter.HiepTHbTestFilter;
import com.example.hieptran.cameraexample.filter.IFInkwellFilter;
import com.example.hieptran.cameraexample.filter.SepiaFilter;
import com.example.hieptran.cameraexample.filter.ToneCurveFilter;
import com.example.hieptran.cameraexample.filter.VignetteFilter;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

import static android.opengl.EGL14.EGL_CONTEXT_CLIENT_VERSION;

//TODO: HiepTHb MiniProject


public class MainActivity extends AppCompatActivity implements
        TextureView.SurfaceTextureListener{
    private static final String TAG = "HiepTHb";
    private TextureView mTextureView;
    private SurfaceTexture mSurfaceTexture;
    private ImageView mImageView1, mImageView2, mImageView3, mImageView4, mImageView5;
    private Camera mCamera;
    //HiepTHb: Su dung cho filter
    ToneCurveFilter toneCurveFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextureView = (TextureView) findViewById(R.id.textureView);
        mImageView1 = (ImageView) findViewById(R.id.imageView1);
        mImageView1.setRotation(90.0f);
        mImageView2 = (ImageView) findViewById(R.id.imageView2);
        mImageView2.setRotation(90.0f);
        mImageView3 = (ImageView) findViewById(R.id.imageView3);
        mImageView3.setRotation(90.0f);
        mImageView4 = (ImageView) findViewById(R.id.imageView4);
        mImageView4.setRotation(90.0f);
        mImageView5 = (ImageView) findViewById(R.id.imageView5);
        mImageView5.setRotation(90.0f);
        mTextureView.setSurfaceTextureListener(this);
        try {
            InputStream is = getApplicationContext().getAssets().open("7_zidi.acv");
            toneCurveFilter = new ToneCurveFilter();
            toneCurveFilter.setFromCurveFileInputStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        mCamera = Camera.open();
        this.mSurfaceTexture = surfaceTexture;
        try {
            mCamera.setPreviewTexture(surfaceTexture);
            mCamera.startPreview();
            mTextureView.setRotation(90.0f);
        }catch (IOException ioe) {
            // Something bad happened
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        mCamera.stopPreview();
        mCamera.release();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                mImageView1.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap;
                        BaseFilter filter = new SepiaFilter();
                        filter.setAsStatic();
                        FilterRender filterRender = new FilterRender(filter);
                        bitmap = filterRender.getFilterBitmap(mTextureView.getBitmap(170, 170));
                        mImageView1.setImageBitmap(bitmap);
                    }
                });
                mImageView2.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap;
                        BaseFilter filter = new BeautyFilter(getResources(), 5);
                        filter.setAsStatic();
                        FilterRender filterRender = new FilterRender(filter);
                        bitmap = filterRender.getFilterBitmap(mTextureView.getBitmap(170, 170));
                        mImageView2.setImageBitmap(bitmap);
                    }
                });
                mImageView4.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap;
                        BaseFilter filter = new VignetteFilter();
                        filter.setAsStatic();
                        FilterRender filterRender = new FilterRender(filter);

                        bitmap = filterRender.getFilterBitmap(mTextureView.getBitmap(170, 170));
                        mImageView4.setImageBitmap(bitmap);
                    }
                });
                mImageView5.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap;
                        BaseFilter filter = new HiepTHbTestFilter(getApplicationContext(), getResources());
                        filter.setAsStatic();
                        FilterRender filterRender = new FilterRender(filter);
                        bitmap = filterRender.getFilterBitmap(mTextureView.getBitmap(170, 170));
                        mImageView5.setImageBitmap(bitmap);
                    }
                });
                mImageView3.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap;

                        toneCurveFilter.setAsStatic();
                        FilterRender filterRender = new FilterRender(toneCurveFilter);
                        bitmap = filterRender.getFilterBitmap(mTextureView.getBitmap(170, 170));
                        mImageView3.setImageBitmap(bitmap);
                    }
                });
            }
        });
    }


}
